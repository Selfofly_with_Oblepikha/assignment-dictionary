extern string_equals

global find_word

section .text

;First argument - rdi (0-terminated string)
;Second - rsi (list beginning)

;List structure (a little hint):
;pointer:
;   dq next_element (8 bytes!)
;   db key, 0 (max of 255 chars plus 0-teminator)
;   db value_string, 0 

find_word:

  .find:
  
    cmp rsi, 0
    je .not_found
    
    add rsi, 8
    push rsi
    call string_equals
    pop rsi
    sub rsi, 8
    cmp rax, 0
    jne .done
    
    mov rsi, [rsi]
    jmp .find
    
  .done:
    mov rax, rsi
    ret
    
  .not_found:
    mov rax, 0
    ret
