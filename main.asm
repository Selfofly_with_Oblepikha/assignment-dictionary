%include "lib.inc"
extern find_word

%define BUFFER_SIZE 256
%define ERR_CODE 1
%define NO_ERR_CODE 0


global _start

section .data

  %include "words.inc"
  overflow_error_message: db "Provided string is not acceptable as a key - buffer overflow (255 symbols max).", 10, 0
  not_found_error_message: db "No such key found in the list.", 10, 0
  
section .text
  
_start:
    sub rsp, BUFFER_SIZE
    mov rdi, rsp
    mov rsi, BUFFER_SIZE

    call read_string
    cmp rax, 0
    je .overflow_error
  
    mov rdi, rsp
    mov rsi, top_element
    call find_word
    cmp rax, 0
    je .not_found_error
    
    push rax
    call print_newline
    pop rax
    add rax, 8
    mov rdi, rax
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    call print_newline
    mov rdi, NO_ERR_CODE
    add rsp, BUFFER_SIZE
    call exit
  
  .overflow_error:
    call print_newline
    mov rdi, overflow_error_message
    call print_error
    mov rdi, ERR_CODE
    add rsp, BUFFER_SIZE
    call exit
    
  .not_found_error:
    call print_newline
    mov rdi, not_found_error_message
    call print_error
    mov rdi, ERR_CODE
    add rsp, BUFFER_SIZE
    call exit


; Takes an address of the string in rdi and prints it to stderr
   
print_error:

    xor rax, rax
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rdi, 2
    mov rax, 1
    syscall
    
  .end:
    ret


; Works the same as read_word, but does nothing to spaces of all types
; Takes buffer address in rdi, its size in rsi
; Buffer address is returned in rax, string size in rdx
; 0 in rax if failed
; Adds 0-terminator at the end of the string

read_string:
    
    push rdi
    push rsi

  .next:
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    cmp rax, 0
    je .end
    dec rsi
    mov [rdi], rax
    inc rdi
    cmp rsi, 0
    jne .next
    pop rsi
    pop rdi
    mov rax, 0
    mov rdx, 0  
    ret
    
  .end:
    mov byte[rdi], 0
    pop rdx
    pop rax
    sub rdx, rsi
    ret

