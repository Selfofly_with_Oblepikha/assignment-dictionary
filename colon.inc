%define top_element 0

%macro colon 2
    %2:
    dq top_element
    db %1, 0
    %define top_element %2
%endmacro